﻿using NotePad.baseStream;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotePad
{
    public partial class FrmPath : Form
    {
        public FrmPath()
        {
            InitializeComponent();
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Note nt = new Note();
            nt.MdiParent = this;
            nt.Show();

        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog();

            if(res == DialogResult.OK)
            {
                string filepath = openFileDialog1.FileName;
                SecuentialStream ss = new SecuentialStream(filepath);
               
            }
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int count = this.MdiChildren.Length;
            if(count == 0)
            {
                return;
            }
            DialogResult result = saveFileDialog1.ShowDialog();

            if(result == DialogResult.OK)
            {
                string filepath = saveFileDialog1.FileName;
                SecuentialStream ss = new SecuentialStream(filepath);
                Form activeChild = this.ActiveMdiChild;
                TextBox txtarea = (TextBox)activeChild.Controls[0];
                ss.writeText(txtarea.Text); 
            }
        }
    }
}
