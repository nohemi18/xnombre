﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];
            ProductoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio, p.Sku + " " + p.Nombre);
            }

            EmpleadoModel.Populate();
            foreach (Empleado emp in EmpleadoModel.GetLstEmpleados())
            {
                dsProductos.Tables["Empleado"].Rows.Add(emp.Id, emp.Inss, emp.Cedula, emp.Nombre, emp.Apellido, emp.Direccion, emp.Sexo, emp.Convenconal, emp.Celular, emp.Salario, emp.Nombre + " " + emp.Apellido);
            }

            new CLienteModel().Populate();
            //foreach (Cliente cli in new CLienteModel().GetLstCliente())
            //{
            //    dsProductos.Tables["Cliente"].Rows.Add(cli.Id, cli.Cedula, cli.Nombres, cli.Apellidos, cli.Telefono, cli.Correo, cli.Direccion);
            //}
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void nuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }
        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionCliente gcl = new FrmGestionCliente();
            gcl.MdiParent = this;
            gcl.DsCliente = dsProductos;
            gcl.Show();
        }
    }
}
