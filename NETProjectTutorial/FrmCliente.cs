﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblCliente;
        private DataSet dsCliente;
        private BindingSource bsCliente;
        private DataRow drCliente;
        public FrmCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        public DataTable TblCliente { set { tblCliente = value; } }
        public DataSet DsCliente { set { dsCliente = value; } }
        public DataRow DrCliente
        { set
            {
                drCliente = value;
                mskced.Text = drCliente["Cedula"].ToString();
                txtnom.Text = drCliente["Nombre"].ToString();
                txtape.Text = drCliente["Apellido"].ToString();
                msktelf.Text = drCliente["Telefono"].ToString();
                txtcorr.Text = drCliente["Correo"].ToString();
                txtdir.Text = drCliente["Direccion"].ToString();
        }   }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = dsCliente;
            bsCliente.DataMember = dsCliente.Tables["Cliente"].TableName;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            string cedula, nombre, apellido, telefono, correo, direccion;

            cedula = mskced.Text;
            nombre = txtnom.Text;
            apellido = txtape.Text;
            telefono = msktelf.Text;
            correo = txtcorr.Text;
            direccion = txtdir.Text;

            if(drCliente != null)
            {
                DataRow dr = tblCliente.NewRow();
                int index = tblCliente.Rows.IndexOf(drCliente);
                dr["ID"] = drCliente["ID"];
                dr["Cedula"] = cedula;
                dr["Nombre"] = nombre;
                dr["Apellido"] = apellido;
                dr["Telefono"] = telefono;
                dr["Correo"] = correo;
                dr["Direccion"] = direccion;
                tblCliente.Rows.RemoveAt(index);
                tblCliente.Rows.InsertAt(dr, index);
            }
            else
            {
                tblCliente.Rows.Add(tblCliente.Rows.Count + 1, cedula, nombre, apellido, telefono, correo, direccion);
            }
            Dispose();
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
