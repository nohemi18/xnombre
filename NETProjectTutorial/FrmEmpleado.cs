﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmEmpleado : Form
    {
        private DataTable tblEmpleados;
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        private DataRow drEmpleado;
        public FrmEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public DataTable TblEmpleados { set { tblEmpleados = value; } }
        public DataSet DsEmpleados { set { dsEmpleados = value; } }
        public DataRow DrEmpleado
        {
            set
            {
                drEmpleado = value;
                mskinss.Text = drEmpleado["INSS"].ToString();
                mskcedula.Text = drEmpleado["Cedula"].ToString();
                txtnombre.Text = drEmpleado["Nombre"].ToString();
                txtapellido.Text = drEmpleado["Apellido"].ToString();
                txtdireccion.Text = drEmpleado["Direccion"].ToString();
                msktelf.Text = drEmpleado["Telefono"].ToString();
                mskcell.Text = drEmpleado["Celular"].ToString();
                msksalario.Text = drEmpleado["Salario"].ToString();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string inss, cedula, nombre, apellido, direccion, convencional, celular;
            double salario;

            inss = mskinss.Text;
            cedula = mskcedula.Text;
            nombre = txtnombre.Text;
            apellido = txtapellido.Text;
            direccion = txtdireccion.Text;
            convencional = msktelf.Text;
            celular = mskcell.Text;
            bool result = Double.TryParse(msksalario.Text, out salario);

            if(drEmpleado != null)
            {
                DataRow drnew = tblEmpleados.NewRow();
                int index = tblEmpleados.Rows.IndexOf(drEmpleado);
                drnew["ID"] = drEmpleado["ID"];
                drnew["INSS"] = inss;
                drnew["Cedula"] = cedula;
                drnew["Nombre"] = nombre;
                drnew["Apellido"] = apellido;
                drnew["Direccion"] = direccion;
                drnew["Telefono"] = convencional;
                drnew["Celular"] = celular;
                drnew["Salario"] = salario;

                tblEmpleados.Rows.RemoveAt(index);
                tblEmpleados.Rows.InsertAt(drnew, index);
            }
            else
            {
                tblEmpleados.Rows.Add(tblEmpleados.Rows.Count + 1, inss, cedula, nombre, apellido, direccion, convencional, celular, salario);
            }
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
        }
    }
}
