﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        public FrmGestionFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsSistema { set { dsSistema = value; } }

        private void btnew_Click(object sender, EventArgs e)
        {
            FrmFactura fac = new FrmFactura();
            fac.DsSistema = dsSistema;
            fac.ShowDialog();
        }

        private void FrmGestionFactura_Load(object sender, EventArgs e)
        {
            bsProductoFactura.DataSource = dsSistema;

        }
    }
}
