﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class CLienteModel
    {
        private static List<Cliente> LstCliente = new List<Cliente>();
        private implements.DaoImplementsCliente daocliente;

        public CLienteModel()
        {
            daocliente = new implements.DaoImplementsCliente();
        }

        public List<Cliente> GetLstCliente()
        {
            return daocliente.findAll();
        }
        public void Populate()
        {
            Cliente[] cliente =
            {
                new Cliente(1,"001-101088-1040X", "Lucas", "Pelucas", "8896-6958", "lucaspel@gmail.com", "La Calzada"),
                new Cliente(2,"001-200699-10785M", "Esteban", "Liquito", "8838-1457", "calorias.no@gmail.com", "Barrio La primavera")
            };
            LstCliente = cliente.ToList();
            foreach(Cliente c in LstCliente)
            {
                daocliente.save(c);
            }
        }
    }
}
