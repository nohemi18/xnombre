﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsCliente;
        private BindingSource bsCliente;
        public FrmGestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        public DataSet DsCliente { set { dsCliente = value; } }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = dsCliente;
            bsCliente.DataMember = dsCliente.Tables["Cliente"].TableName;
            dgvCliente.DataSource = bsCliente;
            dgvCliente.AutoGenerateColumns = true;
        }

        private void btnnew_Click(object sender, EventArgs e)
        {
            FrmCliente clien = new FrmCliente();
            clien.TblCliente = dsCliente.Tables["Cliente"];
            clien.DsCliente = dsCliente;
            clien.ShowDialog();
        }

        private void btneidt_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection row = dgvCliente.SelectedRows;
            if(row.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila para editar", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridrow = row[0];
            DataRow drow = ((DataRowView)gridrow.DataBoundItem).Row;

            FrmCliente cl = new FrmCliente();
            cl.TblCliente = dsCliente.Tables["Cliente"];
            cl.DsCliente = dsCliente;
            cl.DrCliente = drow;
            cl.ShowDialog();
        }

        private void btnelim_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection row = dgvCliente.SelectedRows;
            if (row.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila para eliminar", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = row[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if( result == DialogResult.Yes)
            {
                dsCliente.Tables["Cliente"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
